(function($) {
    $(document).ready(function(){
        window.adjustHeight = function(min_height) {
            var MIN_HEIGHT = min_height || 800;
            var windowHeight = $(window).outerHeight() - 5;
            var siteHeight = $('.layout-base').outerHeight();
            var leftPanel = $('.content__side-panel');
            var workspace = $('.workspace');
            var workspaceBody = $('.workspace__body');

            /* Stretch site by height */
            if (siteHeight < windowHeight) {
                var layoutBodyHeight = $('.layout-base__body').outerHeight(),
                    delta = windowHeight - siteHeight,
                    contentHeight = $('.layout-base__body .content').height(),
                    contentWorkspaceHeight = $('.content__workspace').height();

                $('.layout-base').outerHeight(windowHeight);
                $('.layout-base__body').outerHeight(layoutBodyHeight + delta);
                $('.content').height(contentHeight + delta);
                $('.content__workspace').height(contentWorkspaceHeight + delta);
                workspace.height(workspace.height() + delta);
               workspaceBody.height(workspaceBody.height() + delta);
            }

            /* Cut height */
            if (siteHeight > windowHeight && siteHeight > MIN_HEIGHT) {
                workspaceBody.height(workspaceBody.height() - (siteHeight - Math.max(MIN_HEIGHT, windowHeight)) + 5);
            }

            /* Align left-panel and workspace by height */
            if (leftPanel.outerHeight() > workspace.outerHeight()) {
                workspaceBody.height(workspaceBody.height() +
                    (leftPanel.outerHeight() - workspace.outerHeight()));
            }
        };
        window.adjustHeight();
        $(window).resize(function(){ window.adjustHeight(); });
    });
})(window.jQuery);
