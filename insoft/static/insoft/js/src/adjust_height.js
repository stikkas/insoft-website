/*
 (function ($) {
 $(document).ready(function () {
 window.adjustHeight = function (min_height) {
 var MIN_HEIGHT = min_height || 800,
 windowHeight = $(window).outerHeight() - 5,
 site = $('.layout-base'),
 siteBody = $('.layout-base__body'),
 workspaceContent = $('.content__workspace'),
 content = $('.layout-base__body .content'),
 siteHeight = site.outerHeight(),
 leftPanel = $('.content__side-panel'),
 workspace = $('.workspace'),
 workspaceBody = $('.workspace__body');

 /* Stretch site by height */
/*
 if (siteHeight < windowHeight) {
 var layoutBodyHeight = siteBody.outerHeight(),
 delta = windowHeight - siteHeight,
 contentHeight = content.height(),
 contentWorkspaceHeight = workspaceContent.height();

 site.outerHeight(windowHeight);
 siteBody.outerHeight(layoutBodyHeight + delta);
 content.height(contentHeight + delta);
 workspaceContent.height(contentWorkspaceHeight + delta);
 workspace.height(workspace.height() + delta);
 workspaceBody.height(workspaceBody.height() + delta);
 }

 if (siteHeight > windowHeight && siteHeight > MIN_HEIGHT) {
 console.log("hello");
 var myheight = Math.max(MIN_HEIGHT, windowHeight),
 delta1 = siteHeight - myheight;
 site.outerHeight(myheight);
 //     siteBody.outerHeight(siteBody.outerHeight() - delta1);
 //     content.height(content.height() - delta1);
 //     workspaceContent.height(workspaceContent.height() - delta1);
 workspace.height(workspace.height() - delta1);
 workspaceBody.height(workspaceBody.height() - delta1);
 }
 /* Align left-panel and workspace by height */
/*
 if (leftPanel.outerHeight() > workspace.outerHeight()) {
 workspaceBody.height(workspaceBody.height() +
 (leftPanel.outerHeight() - workspace.outerHeight()));
 }
 };
 window.adjustHeight();
 $(window).resize(function () {
 window.adjustHeight();
 });
 });
 })(window.jQuery);
 */

(function ($) {
    $(document).ready(function () {
        window.adjustHeight = function (min_height) {
            var MIN_HEIGHT = min_height || 900;
            var windowHeight = $(window).outerHeight() - 1;
            var siteHeight = $('.layout-base').outerHeight();
            var leftPanel = $('.content__side-panel');
            var workspace = $('.workspace');
            var workspaceBody = $('.workspace__body');

            /* Stretch site by height */
            if (siteHeight < windowHeight) {
                var xd1 = windowHeight - siteHeight;
                workspace.height(workspace.height() + xd1);
                workspaceBody.height(workspaceBody.height() + xd1);
            }

            /* Cut height */
            if (siteHeight > windowHeight && siteHeight > MIN_HEIGHT) {
                if (MIN_HEIGHT > windowHeight) {
                    var xd2 = 68 - (siteHeight - MIN_HEIGHT);
                    workspace.height(workspace.height() + xd2);
                    workspaceBody.height(workspaceBody.height() + xd2);
                } else {
                    var xd3 = 40 - (siteHeight - windowHeight);
                    workspace.height(workspace.height() + xd3);
                    workspaceBody.height(workspaceBody.height() + xd3);
                }
            }

            /* Align left-panel and workspace by height */
            if (leftPanel.outerHeight() > workspace.outerHeight()) {
                var xd4 = leftPanel.outerHeight() - workspace.outerHeight();
                workspace.outerHeight(workspace.outerHeight() + xd4);
                workspaceBody.height(workspaceBody.height() + xd4);
            }

        };
        window.adjustHeight();
        $(window).resize(function () {
            window.adjustHeight();
        });
    });
})(window.jQuery);
