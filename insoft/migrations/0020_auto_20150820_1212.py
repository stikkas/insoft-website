# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0011_equate_slug_length_with_title_length'),
        ('wagtailredirects', '0001_initial'),
        ('wagtailforms', '0001_initial'),
        ('wagtailsearch', '0001_initial'),
        ('insoft', '0019_auto_20150819_1032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productlinkpage',
            name='link',
        ),
        migrations.RemoveField(
            model_name='productlinkpage',
            name='page_ptr',
        ),
        migrations.DeleteModel(
            name='ProductLinkPage',
        ),
        migrations.RemoveField(
            model_name='productpage',
            name='page_ptr',
        ),
        migrations.DeleteModel(
            name='ProductPage',
        ),
    ]
