# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0011_equate_slug_length_with_title_length'),
        ('wagtailredirects', '0001_initial'),
        ('wagtailforms', '0001_initial'),
        ('wagtailsearch', '0001_initial'),
        ('insoft', '0011_productresheniapage_productuslugipage'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productcategorypage',
            name='page_ptr',
        ),
        migrations.DeleteModel(
            name='ProductCategoryPage',
        ),
        migrations.RemoveField(
            model_name='productsubcategorypage',
            name='page_ptr',
        ),
        migrations.DeleteModel(
            name='ProductSubCategoryPage',
        ),
    ]
