# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('insoft', '0015_auto_20150729_1828'),
    ]

    operations = [
        migrations.AddField(
            model_name='productresheniapage',
            name='content',
            field=wagtail.wagtailcore.fields.RichTextField(default='Empty', verbose_name='Content'),
            preserve_default=False,
        ),
    ]
