# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0011_equate_slug_length_with_title_length'),
        ('insoft', '0010_auto_20150624_1959'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductResheniaPage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('content', wagtail.wagtailcore.fields.RichTextField(null=True, verbose_name='Content', blank=True)),
            ],
            options={
                'db_table': 'insoft_product_reshenia_page',
                'verbose_name': 'Product reshenia page',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='ProductUslugiPage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('content', wagtail.wagtailcore.fields.RichTextField(null=True, verbose_name='Content', blank=True)),
            ],
            options={
                'db_table': 'insoft_product_uslugi_page',
                'verbose_name': 'Product uslugi page',
            },
            bases=('wagtailcore.page',),
        ),
    ]
