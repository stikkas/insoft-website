# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0011_equate_slug_length_with_title_length'),
        ('wagtailredirects', '0001_initial'),
        ('wagtailforms', '0001_initial'),
        ('wagtailsearch', '0001_initial'),
        ('insoft', '0009_productresheniapage_productuslugipage'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productresheniapage',
            name='simplepage_ptr',
        ),
        migrations.DeleteModel(
            name='ProductResheniaPage',
        ),
        migrations.RemoveField(
            model_name='productuslugipage',
            name='simplepage_ptr',
        ),
        migrations.DeleteModel(
            name='ProductUslugiPage',
        ),
    ]
