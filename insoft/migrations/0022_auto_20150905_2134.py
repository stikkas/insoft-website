# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('insoft', '0021_auto_20150905_2047'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'insoft_customer_link',
                'verbose_name': 'Customer Link',
            },
            bases=(models.Model,),
        ),
        migrations.RunSQL("insert into insoft_customer_link values(1);"),
        migrations.AddField(
            model_name='customer',
            name='parent',
            field=modelcluster.fields.ParentalKey(related_name='customer_links', default=1, to='insoft.CustomerLink'),
            preserve_default=True,
        ),
    ]
