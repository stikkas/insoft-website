# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('insoft', '0008_productcategorypage_content'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductResheniaPage',
            fields=[
                ('simplepage_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='insoft.SimplePage')),
            ],
            options={
                'db_table': 'insoft_product_reshenia_page',
                'verbose_name': 'Product reshenia page',
            },
            bases=('insoft.simplepage',),
        ),
        migrations.CreateModel(
            name='ProductUslugiPage',
            fields=[
                ('simplepage_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='insoft.SimplePage')),
            ],
            options={
                'db_table': 'insoft_product_uslugi_page',
                'verbose_name': 'Product uslugi page',
            },
            bases=('insoft.simplepage',),
        ),
    ]
