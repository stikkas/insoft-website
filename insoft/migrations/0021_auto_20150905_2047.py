# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('insoft', '0020_auto_20150820_1212'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='page',
            field=models.ForeignKey(related_name='customer_links', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Customer Page', blank=True, to='insoft.CustomerPage', null=True),
            preserve_default=True,
        ),
    ]
