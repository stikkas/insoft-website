# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('insoft', '0016_productresheniapage_content'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productresheniapage',
            name='content',
            field=wagtail.wagtailcore.fields.RichTextField(null=True, verbose_name='Content', blank=True),
            preserve_default=True,
        ),
    ]
