# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0005_make_filter_spec_unique'),
        ('wagtailcore', '0011_equate_slug_length_with_title_length'),
        ('insoft', '0022_auto_20150905_2134'),
    ]

    operations = [
        migrations.CreateModel(
            name='CareerCrewPage',
            fields=[
                ('page_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('content', wagtail.wagtailcore.fields.RichTextField(null=True, verbose_name='Content', blank=True)),
            ],
            options={
                'db_table': 'insoft_career_crew_page',
                'verbose_name': 'Career Crew Page',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='CareerCrewPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
                ('page', modelcluster.fields.ParentalKey(related_name='photos', to='insoft.CareerCrewPage')),
                ('scan', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name=b'\xd0\xa4\xd0\xbe\xd1\x82\xd0\xbe', blank=True, to='wagtailimages.Image', null=True)),
            ],
            options={
                'ordering': ['sort_order'],
                'db_table': 'insoft_career_crew_photo',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='customerlink',
            options={'verbose_name': '\u0417\u0430\u043a\u0430\u0437\u0447\u0438\u043a', 'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u0447\u0438\u043a\u0438'},
        ),
    ]
