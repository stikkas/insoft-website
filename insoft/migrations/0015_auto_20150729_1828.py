# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0011_equate_slug_length_with_title_length'),
        ('insoft', '0014_auto_20150728_1755'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productresheniapage',
            name='content',
        ),
        migrations.AddField(
            model_name='productresheniapage',
            name='header',
            field=models.CharField(max_length=255, null=True, verbose_name='Header', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='productresheniapage',
            name='link',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Link', blank=True, to='wagtailcore.Page', null=True),
            preserve_default=True,
        ),
    ]
