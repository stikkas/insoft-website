# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields
import modelcluster.fields
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0005_make_filter_spec_unique'),
        ('wagtaildocs', '0002_initial_data'),
        ('insoft', '0018_auto_20150803_1710'),
    ]

    operations = [
        migrations.CreateModel(
            name='UslugiMaterial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
                ('icon', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Icon', blank=True, to='wagtailimages.Image', null=True)),
                ('link', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='File', blank=True, to='wagtaildocs.Document', null=True)),
                ('page', modelcluster.fields.ParentalKey(related_name='materials', to='insoft.ProductUslugiPage')),
            ],
            options={
                'ordering': ['sort_order'],
                'db_table': 'insoft_uslugi_material',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UslugiPatent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
                ('page', modelcluster.fields.ParentalKey(related_name='patents', to='insoft.ProductUslugiPage')),
                ('patent', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Patent', blank=True, to='wagtailimages.Image', null=True)),
            ],
            options={
                'ordering': ['sort_order'],
                'db_table': 'insoft_uslugi_patent',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='productuslugipage',
            name='excerpt',
            field=wagtail.wagtailcore.fields.RichTextField(null=True, verbose_name='Excerpt', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='productuslugipage',
            name='header',
            field=models.CharField(max_length=255, null=True, verbose_name='Header', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='productuslugipage',
            name='icon',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Icon', blank=True, to='wagtailimages.Image', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='productuslugipage',
            name='link',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Link', blank=True, to='insoft.ProductUslugiPage', null=True),
            preserve_default=True,
        ),
    ]
