# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0005_make_filter_spec_unique'),
        ('insoft', '0012_auto_20150626_1048'),
    ]

    operations = [
        migrations.AddField(
            model_name='direction',
            name='icon_glow',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Icon For Hover', blank=True, to='wagtailimages.Image', null=True),
            preserve_default=True,
        ),
    ]
