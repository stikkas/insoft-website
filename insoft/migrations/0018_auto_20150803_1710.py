# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0005_make_filter_spec_unique'),
        ('insoft', '0017_auto_20150729_1909'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResheniaPatent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
                ('page', modelcluster.fields.ParentalKey(related_name='patents', to='insoft.ProductResheniaPage')),
                ('patent', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Patent', blank=True, to='wagtailimages.Image', null=True)),
            ],
            options={
                'ordering': ['sort_order'],
                'db_table': 'insoft_reshenia_patent',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='productresheniapage',
            name='patent',
        ),
        migrations.AlterField(
            model_name='productresheniapage',
            name='link',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Link', blank=True, to='insoft.ProductResheniaPage', null=True),
            preserve_default=True,
        ),
    ]
