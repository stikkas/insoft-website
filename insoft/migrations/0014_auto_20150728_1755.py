# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import wagtail.wagtailcore.fields
import modelcluster.fields
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0005_make_filter_spec_unique'),
        ('wagtaildocs', '0002_initial_data'),
        ('insoft', '0013_direction_icon_glow'),
    ]

    operations = [
        migrations.CreateModel(
            name='ResheniaMaterial',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort_order', models.IntegerField(null=True, editable=False, blank=True)),
                ('icon', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Icon', blank=True, to='wagtailimages.Image', null=True)),
                ('link', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='File', blank=True, to='wagtaildocs.Document', null=True)),
                ('page', modelcluster.fields.ParentalKey(related_name='materials', to='insoft.ProductResheniaPage')),
            ],
            options={
                'ordering': ['sort_order'],
                'db_table': 'insoft_reshenia_material',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='productresheniapage',
            name='excerpt',
            field=wagtail.wagtailcore.fields.RichTextField(null=True, verbose_name='Excerpt', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='productresheniapage',
            name='icon',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Icon', blank=True, to='wagtailimages.Image', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='productresheniapage',
            name='patent',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Patent', blank=True, to='wagtailimages.Image', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='productresheniapage',
            name='content',
            field=wagtail.wagtailcore.fields.RichTextField(default='Default Content', verbose_name='Content'),
            preserve_default=False,
        ),
    ]
