# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('insoft', '0023_auto_20150930_1656'),
    ]

    operations = [
        migrations.RenameField(
            model_name='careercrewphoto',
            old_name='scan',
            new_name='photo',
        ),
    ]
