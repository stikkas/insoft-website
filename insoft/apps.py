from django.apps import AppConfig
from django.db.models import signals
from insoft.share import fill_ru_menu, fill_clients, fill_customers
from insoft.models import (ProductUslugiPage, ProductResheniaPage, Customer,
                           CustomerPage)


class InsoftAppConfig(AppConfig):
    name = 'insoft'
    label = 'insoft'

    def ready(self):
        signals.post_save.connect(fill_ru_menu, ProductResheniaPage)
        signals.post_save.connect(fill_ru_menu, ProductUslugiPage)
        signals.post_delete.connect(fill_ru_menu, ProductResheniaPage)
        signals.post_delete.connect(fill_ru_menu, ProductUslugiPage)

        signals.post_save.connect(fill_clients, Customer)
        signals.post_delete.connect(fill_clients, Customer)

        signals.post_save.connect(fill_customers, CustomerPage)
        signals.post_delete.connect(fill_customers, CustomerPage)

        fill_ru_menu()
        fill_clients()
        fill_customers()
