
# encoding: utf-8
import datetime
import time

from django.conf.urls import url
from django.conf import settings
from django.db import models
from django.http import Http404, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render

from wagtail.wagtailsnippets.models import register_snippet
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailadmin.edit_handlers import (FieldPanel, InlinePanel,
                                                PageChooserPanel)

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from wagtail.wagtailsearch import index
from wagtail.contrib.wagtailroutablepage.models import RoutablePageMixin
from modelcluster.fields import ParentalKey
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase, Tag

from insoft import maps

HEADLINE_LEN = 100
LAST_ENTRIES_ON_PAGE = 20

PAGES_FOR_SEARCH = []

def reg_cls_for_search(*fields):
    '''Добавляет класс в PAGES_FOR_SEARCH
        с указанием полей для поиска, по полю
        title ищется всегда, поэтому его здесь не указываем'''
    def wrapper(cls):
        PAGES_FOR_SEARCH.append({'cls': cls, 'fields': fields})
        return cls
    return wrapper


class Direction(Orderable):
    page = ParentalKey('insoft.StartPage', related_name='directions')
    text = RichTextField(_('Direction'), blank=True, null=True)
    link = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+'
    )
    icon = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Icon'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    icon_glow = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Icon For Hover'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    panels = [
        FieldPanel('text', classname='full'),
        PageChooserPanel('link'),
        ImageChooserPanel('icon'),
        ImageChooserPanel('icon_glow'),
    ]

    class Meta:
        db_table = 'insoft_start_direction'
        ordering = ['sort_order']

# Start page
@reg_cls_for_search('content')
class StartPage(Page):
    headline = models.CharField(_('Headline'), max_length=HEADLINE_LEN, blank=True, null=True)
    content = RichTextField(_('Content'), blank=True, null=True)

    class Meta:
        db_table = 'insoft_start_page'
        verbose_name = _('Start page')

StartPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('headline', classname='full title'),
    FieldPanel('content', classname='full'),
    InlinePanel(StartPage, 'directions', label=_('Directions')),
]


# Simple page
@reg_cls_for_search('content')
class SimplePage(Page):
    redirect_urls = ['/formula/', '/about/', '/career/']
    headline = models.CharField(_('Headline'), max_length=HEADLINE_LEN, blank=True, null=True)
    content = RichTextField(_('Content'), blank=True, null=True)

    def serve(self, request, *args, **kwargs):
        if self.url in self.redirect_urls:
            return HttpResponseRedirect(self.get_children().live()[0].url)
        return super(SimplePage, self).serve(request, *args, **kwargs)

    class Meta:
        db_table = 'insoft_simple_page'
        verbose_name = _('Simple page')

SimplePage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('headline', classname='full title'),
    FieldPanel('content', classname='full'),
]


# Страница для "Команда" в "Карьера"
class CareerCrewPhoto(Orderable):
    page = ParentalKey('insoft.CareerCrewPage', related_name='photos')
    photo = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name='Фото',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    panels = [
        ImageChooserPanel('photo')
    ]

    class Meta:
        db_table = 'insoft_career_crew_photo'
        ordering = ['sort_order']


@reg_cls_for_search('content')
class CareerCrewPage(Page):
    content = RichTextField(_('Content'), blank=True, null=True)

    class Meta:
        db_table = 'insoft_career_crew_page'
        verbose_name = _('Career Crew Page')

CareerCrewPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('content', classname='full'),
    InlinePanel(CareerCrewPage, 'photos', label="Фотографии")
]

# Страница 'Портрет Сотрудника'
class EmployeePage(Page):
    background = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Background'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    foreground = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Foreground'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    class Meta:
        db_table = 'insoft_employee_page'
        verbose_name = _('Employee page')

EmployeePage.content_panels = [
    FieldPanel('title', classname='full title'),
    ImageChooserPanel('background'),
    ImageChooserPanel('foreground'),
]

# Chronology
class ChronologyRecord(Orderable):
    page = ParentalKey('insoft.ChronologyPage', related_name='records')
    period = models.CharField(_('Period'), max_length=20)
    record = RichTextField(_('Record'))

    panels = [
        FieldPanel('period'),
        FieldPanel('record')
    ]

    class Meta:
        db_table = 'insoft_chronology_record'
        ordering = ['sort_order']


class ChronologyPage(Page):
    subpage_types = []

    headline = models.CharField(_('Headline'), max_length=HEADLINE_LEN, blank=True, null=True)
    preface = RichTextField(_('Preface'), blank=True, null=True)

    class Meta:
        db_table = 'insoft_chronology_page'
        verbose_name = _('Chronology')

ChronologyPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('headline', classname='full title'),
    FieldPanel('preface', classname='full'),
    InlinePanel(ChronologyPage, 'records', label=_('Records')),
]


# Crew
@reg_cls_for_search('bio')
class CrewmanPage(Page):
    subpage_types = []

    name = models.CharField(_('Name'), max_length=100)
    face = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Face'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    position = models.CharField(_('Position'), max_length=100)
    bio = RichTextField(_('Biography'))

    class Meta:
        db_table = 'insoft_crewman_page'
        verbose_name = _('Crewman')

CrewmanPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('name', classname='full title'),
    ImageChooserPanel('face'),
    FieldPanel('position', classname='full title'),
    FieldPanel('bio', classname='full')
]


class CrewPage(Page):
    subpage_types = ['insoft.CrewmanPage']

    headline = models.CharField(_('Headline'), max_length=HEADLINE_LEN, blank=True, null=True)

    @property
    def members(self):
        return CrewmanPage.objects.filter(live=True)

    class Meta:
        db_table = 'insoft_crew_page'
        verbose_name = _('Crew')

CrewPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('headline', classname='full title'),
]


# Official Documents
class OfficialDocumentsPage(Page):
    subpage_types = ['insoft.DocumentsCategoryPage']

    @property
    def categories(self):
        return DocumentsCategoryPage.objects.child_of(self)

    class Meta:
        db_table = 'insoft_official_documents_page'
        verbose_name = _('Official documents')


class DocumentsCategoryPage(Page):
    subpage_types = ['insoft.DocumentPage']

    @property
    def documents(self):
        return DocumentPage.objects.child_of(self).prefetch_related('scans__scan')

    class Meta:
        db_table = 'insoft_documents_category_page'
        verbose_name = _('Documents category')


class DocumentScan(Orderable):
    page = ParentalKey('insoft.DocumentPage', related_name='scans')
    scan = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Scan'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    panels = [
        ImageChooserPanel('scan')
    ]

    class Meta:
        db_table = 'insoft_document_scan'
        ordering = ['sort_order']


@reg_cls_for_search('description')
class DocumentPage(Page):
    subpage_types = []
    description = models.CharField(_('Description'), max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'insoft_document_page'
        verbose_name = _('Document')

DocumentPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('description', classname='full'),
    InlinePanel(DocumentPage, 'scans', label=_('Scans'))
]


# Customers
class CustomersPage(RoutablePageMixin, Page):
    subpage_types = ['insoft.CustomerPage']
    subpage_urls = (
        url(r'^$', 'map', name='customers_map'),
        url(r'^location/(?P<location_code>ru-mow|ru-spe)/$',
            'fed_cust_on_location', name='customers_on_location'),
        url(r'^location/(?P<location_code>[a-z-]{2,10})/$',
            'customers_on_location', name='customers_on_location'),
    )

    def map(self, request):
        from insoft import share
        return render(request, self.template, {
                      'self': self,
                      'base_color': settings.COLOR_REGION_WITHOUT_CUSTOMERS,
                      'colors': share.CUSTOMERS})

    # В Москве и Питере несколько страниц
    def fed_cust_on_location(self, request, location_code):
        location = maps.get_location(maps.RUSSIA, location_code)
        if not location:
            raise Http404

        customers = CustomerPage.objects.filter(
            live=True,
            location_on_map=location.code
        ).defer('title')
        return self._return_lp(request, customers, location)

    def customers_on_location(self, request, location_code):
        location = maps.get_location(maps.RUSSIA, location_code)
        if not location:
            raise Http404

        customers = CustomerPage.objects.filter(
            live=True,
            location_on_map=location.code,
        ).exclude(content=None).defer('title')

        if len(customers) > 0:
            # На один регион одна страница
            return HttpResponseRedirect(customers[0].relative_url(request.site))

        # Значит в регионе нет заказчиков
        return self._return_lp(request, customers, location)

    def _return_lp(self, request, customers, location):
        return render(request, 'insoft/customers_on_location_page.html', {
            'customers': customers,
            'self': self,
            'location': location
        })

    class Meta:
        db_table = 'insoft_customers_page'
        verbose_name = _('Customers page')


class CustomerRecord(Orderable):
    page = ParentalKey('insoft.CustomerPage', related_name='customers')
    title = models.CharField(_('Title'), blank=True, null=True, max_length=255)
    record = RichTextField(_('Customer'), blank=True, null=True)

    panels = [
        FieldPanel('title', classname='full title'),
        FieldPanel('record', classname='full'),
    ]

    class Meta:
        db_table = 'insoft_customer_record'
        ordering = ['sort_order']


@reg_cls_for_search('content')
class CustomerPage(Page):
    headline = models.CharField('Headline', max_length=HEADLINE_LEN, blank=True, null=True)
    location_on_map = models.CharField('Location on map', max_length=10,
                                       choices=maps.choices(maps.RUSSIA))
    content = RichTextField('Content', blank=True, null=True)

    class Meta:
        db_table = 'insoft_customer_page'
        verbose_name = 'Customer page'

CustomerPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('headline', classname='full title'),
    FieldPanel('location_on_map', classname='full title'),
    FieldPanel('content', classname='full'),
    InlinePanel(CustomerPage, 'customers', label='Customers'),
]


# Customer in footer
class Customer(Orderable):
    parent = ParentalKey(to='insoft.CustomerLink', related_name='customer_links',
                         default=1)
    page = models.ForeignKey(
        'insoft.CustomerPage',
        related_name='customer_links',
        verbose_name=_('Customer Page'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    title = models.CharField(_('Customer Name'), max_length=255)
    emblem = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Customer emblem'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    @property
    def url(self):
        return self.page.url

    panels = [
        PageChooserPanel('page', 'insoft.CustomerPage'),
        FieldPanel('title'),
        ImageChooserPanel('emblem')
    ]

    def __unicode__(self):
        return self.title


@register_snippet
class CustomerLink(models.Model):
    class Meta:
        db_table = 'insoft_customer_link'
        verbose_name = 'Заказчик'
        verbose_name_plural = 'Заказчики'

    def __unicode__(self):
        return u'Список заказчиков'



CustomerLink.panels = [
    InlinePanel(CustomerLink, 'customer_links', label="Заказчики")
]


# Products
@reg_cls_for_search('content')
class ProductsPage(Page):
    subpage_types = [
        'insoft.ProductResheniaPage',
        'insoft.ProductUslugiPage'
    ]
    content = RichTextField(_('Content'), blank=True, null=True)

    def serve(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.get_children().live()[0].url)

    @property
    def products(self):
        return ProductResheniaPage.objects.filter(url_path__regex=r'%s[-a-z]+/$'
                                                                  % self.url_path,
                                                  show_in_menus=True, live=True)

    @property
    def uslugi(self):
        return ProductUslugiPage.objects.filter(url_path__regex=r'%s[-a-z]+/$'
                                                                % self.url_path,
                                                show_in_menus=True, live=True)

    class Meta:
        db_table = 'insoft_products_page'
        verbose_name = _('Products page')

ProductsPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('content', classname='full'),
]


class AbstractMaterial(Orderable):
    """
    Общий класс для материалов страниц "Решения и Услуги"
    """
    icon = models.ForeignKey('wagtailimages.Image', verbose_name=_('Icon'),
                             null=True, blank=True, on_delete=models.SET_NULL,
                             related_name='+')
    link = models.ForeignKey('wagtaildocs.Document', verbose_name=_('File'),
                             null=True, blank=True, on_delete=models.SET_NULL,
                             related_name='+')
    panels = [
        DocumentChooserPanel('link'),
        ImageChooserPanel('icon'),
    ]

    class Meta:
        abstract = True


class ResheniaMaterial(AbstractMaterial):
    """
    Материалы для страниц "Решения"
    """
    page = ParentalKey('insoft.ProductResheniaPage', related_name='materials')

    class Meta:
        db_table = 'insoft_reshenia_material'
        ordering = ['sort_order']


class UslugiMaterial(AbstractMaterial):
    """
    Материалы для страниц "Услуги"
    """
    page = ParentalKey('insoft.ProductUslugiPage', related_name='materials')

    class Meta:
        db_table = 'insoft_uslugi_material'
        ordering = ['sort_order']


class AbstractPatent(Orderable):
    """
    Общий класс для патентов, сертификатов и подобных вещей страниц "Решения и Услуги"
    """
    patent = models.ForeignKey('wagtailimages.Image', verbose_name=_('Patent'),
                             null=True, blank=True, on_delete=models.SET_NULL,
                             related_name='+')
    panels = [
        ImageChooserPanel('patent')
    ]

    class Meta:
        abstract = True

class ResheniaPatent(AbstractPatent):
    """
    Класс для патентов, сертификтов и т.п. страниц "Решения"
    """
    page = ParentalKey('insoft.ProductResheniaPage', related_name='patents')

    class Meta:
        db_table = 'insoft_reshenia_patent'
        ordering = ['sort_order']


class UslugiPatent(AbstractPatent):
    """
    Класс для патентов, сертификтов и т.п. страниц "Услуги"
    """
    page = ParentalKey('insoft.ProductUslugiPage', related_name='patents')

    class Meta:
        db_table = 'insoft_uslugi_patent'
        ordering = ['sort_order']


class AbstractPRUPage:
    """
    Общий класс для страниц "Решения и Услуги"
    """
    header = models.CharField(_('Header'), max_length=255, null=True, blank=True)
    icon = models.ForeignKey('wagtailimages.Image', verbose_name=_('Icon'),
                             null=True, blank=True, on_delete=models.SET_NULL,
                             related_name='+')
    excerpt = RichTextField(_('Excerpt'), null=True, blank=True)
    content = RichTextField(_('Content'), null=True, blank=True)

    @property
    def ancestors(self):
        parents = []
        include = False
        for p in self.get_ancestors():
            if not include:
                if ProductsPage.objects.filter(pk=p.id).exists():
                    include = True
                    parents.append({'title': p.title, 'url': p.url})
            else:
                parents.append({'title': p.title, 'url': p.url})
        parents.append({'title': self.title, 'url': self.url})
        return parents

    @property
    def children(self):
        return [{'title': p.title, 'url': p.url, 'icon': p.icon,
                 'excerpt': p.excerpt, 'link': p.link} for p in
                self.__class__.objects.filter(url_path__regex=r'%s[-a-z]+/$'
                % self.url_path, live=True)]

    @property
    def kids(self):
        return self.__class__.objects.filter(
            url_path__regex=r'%s[-a-z]+/$' % self.url_path, show_in_menus=True,
            live=True)


# Общие значения для заполнения форм страниц "Решения и Услуги"
ResheniaUslugiPanels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('header', classname='full title'),
    # Здесь будет ссылка на другую страницу
    ImageChooserPanel('icon'),
    FieldPanel('excerpt', classname='full'),
    FieldPanel('content', classname='full'),
    # далее идут патенты и материалы
]


@reg_cls_for_search('content')
class ProductResheniaPage(Page, AbstractPRUPage):
    """
    Страницы "Решения"
    """
    subpage_types = ['insoft.ProductResheniaPage']
    link = models.ForeignKey('insoft.ProductResheniaPage', verbose_name=_('Link'),
                            null=True, blank=True, on_delete=models.SET_NULL,
                            related_name='+')
    # Используем эту хрень т.к. админка не определяет поля для родителей, т.е. не может
    # отрисовать в форме поле предка
    header = AbstractPRUPage.header
    icon = AbstractPRUPage.icon
    excerpt = AbstractPRUPage.excerpt
    content = AbstractPRUPage.content

    class Meta:
        db_table = 'insoft_product_reshenia_page'
        verbose_name = _('Product reshenia page')

ProductResheniaPage.content_panels = (ResheniaUslugiPanels[0:2] +
                                      [PageChooserPanel('link', 'insoft.ProductResheniaPage')] +
                                      ResheniaUslugiPanels[2:] + ([
                                          InlinePanel(ProductResheniaPage, 'patents', label=_('Patents')),
                                          InlinePanel(ProductResheniaPage, 'materials', label=_('Materials'))
                                      ]))


@reg_cls_for_search('content')
class ProductUslugiPage(Page, AbstractPRUPage):
    """
    Страницы "Услуги"
    """
    subpage_types = ['insoft.ProductUslugiPage']
    link = models.ForeignKey('insoft.ProductUslugiPage', verbose_name=_('Link'),
                            null=True, blank=True, on_delete=models.SET_NULL,
                            related_name='+')
    header = AbstractPRUPage.header
    icon = AbstractPRUPage.icon
    excerpt = AbstractPRUPage.excerpt
    content = AbstractPRUPage.content

    class Meta:
        db_table = 'insoft_product_uslugi_page'
        verbose_name = _('Product uslugi page')

ProductUslugiPage.content_panels = (ResheniaUslugiPanels[0:2] +
                                    [PageChooserPanel('link', 'insoft.ProductUslugiPage')] +
                                    ResheniaUslugiPanels[2:] + ([
                                        InlinePanel(ProductUslugiPage, 'patents', label=_('Patents')),
                                        InlinePanel(ProductUslugiPage, 'materials', label=_('Materials'))
                                    ]))


# Press
class PressPage(RoutablePageMixin, Page):
    subpage_types = ['insoft.PressEntryPage']
    subpage_urls = (
        url(r'^$', 'latest', name='latest'),
        url(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{1,2})/$', 'archive', name='archive'),
        url(r'^tag/(?P<tag>[0-9]+)/$', 'show_tag', name='show-tag'),
    )

    @property
    def last_entries(self):
        return PressEntryPage.objects.filter(
            live=True,
        ).order_by('-release_date').only('title', 'release_date', 'url_path')[:LAST_ENTRIES_ON_PAGE]

    def latest(self, request):
        return render(request, self.template, {'self': self})

    def archive(self, request, year, month):
        try:
            t = time.strptime('%s-%s' % (year, month), '%Y-%m')
            selected_date = datetime.date(*t[:3])
        except ValueError:
            raise Http404

        entries = PressEntryPage.objects.filter(
            live=True,
            release_date__year=selected_date.year,
            release_date__month=selected_date.month
        )
        if not entries.exists():
            raise Http404

        return render(request, 'insoft/press_archive_page.html', {
            'entries': entries,
            'self': self,
            'selected_date': selected_date
        })

    def show_tag(self, request, tag):
        entries = PressEntryTag.objects.filter(tag_id=tag)
        if not entries.exists():
            raise Http404

        return render(request, 'insoft/press_tag_page.html', {
            'entries': [it.content_object for it in entries],
            'self': self,
            'tag_name': Tag.objects.get(id=tag).name
        })

    class Meta:
        db_table = 'insoft_press_page'
        verbose_name = _('Press page')


class PressEntryTag(TaggedItemBase):
    content_object = ParentalKey('insoft.PressEntryPage', related_name='tagged_items')

    class Meta:
        db_table = 'insoft_press_entry_tags'


@reg_cls_for_search('body')
class PressEntryPage(Page):
    subpage_types = []

    body = RichTextField(_('Body'))
    tags = ClusterTaggableManager(through=PressEntryTag, blank=True)
    release_date = models.DateField(_('Release date'))
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_('Feed image'),
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + (
        index.SearchField('body'),
    )

    class Meta:
        db_table = 'insoft_press_entry_page'
        verbose_name = _('Press entry')

PressEntryPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('release_date'),
    FieldPanel('body', classname='full'),
    ImageChooserPanel('feed_image'),
    FieldPanel('tags')
]


# Contacts
class ContactsOffice(Orderable):
    page = ParentalKey('insoft.ContactsPage', related_name='offices')
    map = models.CharField(_('Map'), max_length=1000)
    info = RichTextField(_('Info'))

    panels = [
        FieldPanel('map', classname='full'),
        FieldPanel('info', classname='full')
    ]

    class Meta:
        db_table = 'insoft_contacts_office'
        ordering = ['sort_order']


class ContactsRequisite(Orderable):
    page = ParentalKey('insoft.ContactsPage', related_name='requisites')
    name = models.CharField(_('Name'), max_length=20)
    value = models.CharField(_('Value'), max_length=255)

    panels = [
        FieldPanel('name', classname='full'),
        FieldPanel('value', classname='full')
    ]

    class Meta:
        db_table = 'insoft_contacts_requisite'
        ordering = ['sort_order']


@reg_cls_for_search('preface')
class ContactsPage(Page):
    preface = RichTextField(_('Preface'), null=True, blank=True)

    class Meta:
        db_table = 'insoft_contacts_page'
        verbose_name = _('Contacts page')

ContactsPage.content_panels = [
    FieldPanel('title', classname='full title'),
    FieldPanel('preface', classname='full'),
    InlinePanel(ContactsPage, 'offices', label=_('Offices')),
    InlinePanel(ContactsPage, 'requisites', label=_('Requisites'))
]


