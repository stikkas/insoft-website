
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'insoft_develop',
        'USER': 'insoft',
        'PASSWORD': 'insoft',
        'HOST': 'localhost',
        'CONN_MAX_AGE': 600,
    }
}

ALLOWED_HOSTS = ['localhost', 'w3.insoft.ru', 'www-new.insoft03.lan', 'w3.insoft03.lan', "druppi.insoft.ru"]
