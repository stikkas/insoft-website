import json

from django.conf import settings
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.db.models import Q
from insoft.models import PAGES_FOR_SEARCH

def search(request, results_per_page=10):
    if hasattr(settings, 'WAGTAILSEARCH_RESULTS_TEMPLATE'):
        template = settings.WAGTAILSEARCH_RESULTS_TEMPLATE
    else:
        template = 'wagtailsearch/search_results.html'

    # Get query string and page from GET paramters
    query_string = request.GET.get('q', '')
    page = int(request.GET.get('p', 1))

    # Search
    search_results = []
    if query_string != '':
        for p_cls in PAGES_FOR_SEARCH:
            filters = Q(title__icontains=query_string)
            for field in p_cls['fields']:
                filters.add(Q(**{('%s__icontains' % field): query_string}), Q.OR)
            res = p_cls['cls'].objects.filter(filters)
            if res:
                search_results += res

        search_results = sorted(search_results, key=lambda p: p.title.lower().count(query_string.lower()),
                                reverse=True)
        # Pagination
        paginator = Paginator(search_results, results_per_page)
        try:
            search_results = paginator.page(page)
        except PageNotAnInteger:
            page = 1
            search_results = paginator.page(page)
        except EmptyPage:
            page = paginator.num_pages
            search_results = paginator.page(page)

        prev_page = page - 1 if page > 1 else None
        next_page = page + 1 if page < paginator.num_pages else None

    return render(request, template, dict(
            query_string=query_string,
            search_results=search_results,
            prev_page=prev_page,
            next_page=next_page
        ))
