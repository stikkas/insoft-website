__author__ = 'basa'
# encoding: utf-8
"""
Общие данные для всего приложения.
Обновление происходит по каким-то событиям, связанным
с редактированием, созданием объектов.
"""
from django.db import connection
from django.conf import settings
from insoft.models import ProductsPage, CustomerPage
from insoft.maps import get_map, RUSSIA

"""
Данные для меню страницы "Решения и Услуги"
"""
RU_MENU = []


def fill_ru_menu(**kwargs):
    """
    Наполняет RU_MENU
    """
    global RU_MENU
    root_page = ProductsPage.objects.all()[0]

    reshenia = []
    uslugi = []

    _get_root_children(root_page.products, reshenia)
    _get_root_children(root_page.uslugi, uslugi)
    RU_MENU = [{'Решения' : reshenia}, {'Услуги': uslugi}]


def _get_root_children(items, result):
    """
    Получает потомков корневого меню
    :param items: список меню первого уровня
    :param result: список для результатов
    """
    for it in items:
        data = {'title': it.title, 'url': it.url}
        _get_children(it)
        if hasattr(it, 'childs'):
            data['children'] = it.childs
        result.append(data)


def _get_children(parent):
    """
    Получает все дочерние страницы для заданной страницы
    :param parent: объект, кого потомков надо найти
    """
    if len(parent.kids) > 0:
        parent.childs = []
        _get_root_children(parent.kids, parent.childs)

"""
Эмблемы заказчиков. Используются в нижней карусели.
"""
CLIENTS = []


def fill_clients(**kwargs):
    """
    Наполняет CLIENTS
    """
    global CLIENTS
    cursor = connection.cursor()

#    cursor.execute("select ic.title as title, substring(wp.url_path from 6) as url, "
#                   "concat('/media/', wi.file) as file, wi.width as width, "
#                   "wi.height as height from insoft_customer ic join "
#                   "wagtailimages_image wi on ic.emblem_id = wi.id join "
#                   "wagtailcore_page wp on ic.page_id=wp.id")
    cursor.execute("select ic.title as title, substring(wp.url_path from 6) as "
                   "url, concat('/media/', wi.file) as file from "
                   "insoft_customer ic join wagtailimages_image wi on "
                   "ic.emblem_id = wi.id join wagtailcore_page wp on "
                   "ic.page_id=wp.id order by ic.sort_order")
    desc = cursor.description
    CLIENTS = [dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()]

"""
Коэффициенты увеличения темноты цвета на карте в соответсвии с регионом
На данном этапе отказались от этого
"""
COLORS = {}


def fill_colors(**kwargs):
    """
        Заполняет коэффициенты заказчиков для регионов,
        чем больше заказчиков, тем больше коэффициент, самый минимальный коэффициент - 0
    """
    global COLORS
    codes = []
    koef = []
    for location in get_map(RUSSIA):
        code = location.code
        pages = CustomerPage.objects.filter(location_on_map=code)
        codes.append(code)
        if pages:
            koef.append(len(pages[0].customers.all()))
        else:
            koef.append(0)
    uniq = sorted(set(koef))
    cases = len(uniq)
    incs = {it[0]: it[1] for it in zip(uniq, list(xrange(0, 51, 50 / cases))[:cases])}
    COLORS = {it[0]: it[1] for it in zip(codes, [incs[val] for val in koef])}


"""
Регионы с наличием заказчиков
"""
CUSTOMERS = {}


def fill_customers(**kwargs):
    """
    Заполняет объект с регионами, у которых есть заказчики
    для отображения их на карте другим цветом
    """
    global CUSTOMERS
    customs = {}
    for location in get_map(RUSSIA):
        code = location.code
        pages = CustomerPage.objects.filter(location_on_map=code, live=True)
        if pages:
            customs[code] = settings.COLOR_REGION_WITH_CUSTOMERS
    CUSTOMERS = customs

