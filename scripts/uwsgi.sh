#!/bin/sh

name=insoft
chroot=$HOME/${name}-website
app_file=${chroot}/wsgi.py
dst_dir=${chroot}/uwsgi
pid_file=$dst_dir/${name}.pid
socket=$dst_dir/${name}.sock
log_file=$dst_dir/${name}.log

insoft_start() {
    uwsgi --chdir ${chroot} -p 4 --socket $socket --chmod-socket=666 --master --pidfile $pid_file --wsgi-file $app_file --daemonize $log_file
}

insoft_stop() {
  uwsgi --stop $pid_file
}

insoft_reload() {
  uwsgi --reload $pid_file
}

check_status() {
  ps ax|grep `cat $pid_file`|grep -v grep > /dev/null 2>&1
}

process() {
  case $1 in 
	start)
	  check_status
  	  if [ $? = 0 ]; then 
    	    echo "process already running"
  	  else
	    ${name}_start
  	  fi
	;;
	stop)
	  ${name}_stop
	;;
	restart)
	  ${name}_stop
	  ${name}_start
	;;
        reload)
	  ${name}_reload
	;;
	*)
	  echo "Usage: $0 {start|stop|restart|reload}"
	  exit 1
	;;
  esac
}

process $1
